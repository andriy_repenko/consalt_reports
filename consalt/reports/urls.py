from django.conf.urls import url
from . import views
from django.urls import include, path
from django.views.generic import TemplateView

app_name = "reports"
urlpatterns = [
    path("", views.HomePageView.as_view(), name='home'),
    path(
        "thank-you/",
        TemplateView.as_view(template_name="pages/thank_you.html"),
        name="thank_you",
    ),

]

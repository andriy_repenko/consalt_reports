from django import forms
from .models import Report


class ReportForm(forms.ModelForm):

    def send_email(self):
        # TODO send email using the self.cleaned_data dictionary
        pass

    class Meta:
        model = Report
        fields = ('store_name', 'date', 'turn', 'period', 'sold', 'fuels')

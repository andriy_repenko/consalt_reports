from django.apps import AppConfig


class ReportsConfig(AppConfig):
    name = 'consalt.reports'

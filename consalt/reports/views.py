from django.shortcuts import render
from .forms import ReportForm
from django.core.mail import send_mail
from django.conf import settings
from django.views.generic.edit import FormView
from django.urls import reverse_lazy
from .models import Report
import pdb


class HomePageView(FormView):
    template_name = 'pages/home_page.html'
    form_class = ReportForm
    success_url = reverse_lazy('reports:thank_you')

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        report = Report.objects.create(store_name=form.cleaned_data['store_name'], date=form.cleaned_data['date'], turn=form.cleaned_data['turn'], period=form.cleaned_data['period'],
                                       sold=form.cleaned_data['sold'], fuels=form.cleaned_data['fuels'])
        report.send_email()
        return super().form_valid(form)




# class HomePageViews(TemplateView):
#     template_name = 'pages/home_page.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(HomePageViews, self).get_context_data(**kwargs)
#         context.update({
#             'form': ReportForm(),
#
#         })
#         return context
#
#     def post(self,request):
#         if request.method == 'POST':
#             pdb.set_trace()
#             form = ReportForm(request.POST)
#             if form.is_valid():
#                 cd = form.cleaned_data
#                 self.send_email(cd['store_name'], cd['date'])
#         else:
#             form = ReportForm()
#
#         return render(request, 'pages/home_page.html', {'form': form})
#
#     def send_email(self, store_name, date):
#         str(date)
#         formatted_date = date.stfrtime('%d %B, %Y')
#         subject = 'Report {0} date {1}'.format(store_name, formatted_date)
#         message = ' it  means a world to us '
#         email_from = settings.EMAIL_HOST_USER
#         recipient_list = ['receiver@gmail.com', ]
#         send_mail(subject, message, email_from, recipient_list)
#         return redirect('redirect to a new page')







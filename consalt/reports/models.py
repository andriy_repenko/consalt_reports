from django.db import models
from django.core.mail import send_mail
from django.conf import settings
import pdb

class Report(models.Model):
    store_name = models.CharField(max_length= 256)
    created_ad = models.DateTimeField(auto_now_add=True,blank=False, null= False)
    date = models.DateField(blank=False, null=False)
    turn = models.IntegerField(blank=False, null=False)
    period = models.FloatField(blank=False, null=False)
    sold = models.IntegerField(blank=False, null=False)
    fuels = models.IntegerField(blank=False, null=False)






    def __str__(self):
        return 'name: {0},  created ad: {1}'.format(self.store_name, self.created_ad)


    def send_email(self):
        date = str(self.date)
        created_date = self.created_ad.strftime('%d %m %Y')
        subject = 'Report {0} date {1}'.format(self.store_name, date)
        message = 'Store Name:  {} \
                   Created Date:  {} \
                   Date:  {} \
                   Turn:  {} \
                   Period:  {} \
                   Sold:  {} \
                   Fuels:  {}'.format(self.store_name, created_date, self.date, self.turn, self.period, self.sold, self.fuels)
        email_from = settings.EMAIL_HOST_USER
        recipient_list = ['Andriy592@gmail.com', ]
        send_mail(subject, message, email_from, recipient_list)
